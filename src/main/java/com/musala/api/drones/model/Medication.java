package com.musala.api.drones.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medications")
public class Medication implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@Column(name = "uid", length = 32, columnDefinition = "uuid NOT NULL", unique = true, nullable = false)
	UUID uuid;

	@Column(nullable = false)
	private String name;

	@Column(name = "weight_gr", nullable = false)
	private int weight;

	@Column(name = "code", nullable = false)
	private String code;

	@Lob
	@Column(name = "image", nullable = false, columnDefinition = "BLOB NOT NULL")
	private byte[] image;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "drone", nullable = false)
	private Drone drone;
}
