package com.musala.api.drones.dto;

import java.util.UUID;

import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@NoArgsConstructor
public class MedicationDTO {
	// TODO: Implement regex patterns for name and coode
	private String name;
	private int weight;
	private String code;
	private UUID droneId;
	private byte[] image;
}
