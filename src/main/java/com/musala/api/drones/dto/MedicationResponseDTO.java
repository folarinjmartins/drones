package com.musala.api.drones.dto;

import java.util.UUID;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MedicationResponseDTO {
	private UUID uuid;
	private String name;
	private int weight;
	private String code;
	private UUID droneId;
	private byte[] image;
}
