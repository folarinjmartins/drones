package com.musala.api.drones.dto;

import org.springframework.stereotype.Component;

import javax.validation.constraints.Max;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
@Component
public class DroneRegistrationDTO {
	@Size(max = 100, min = 1, message = "Character count of 100 exceeded")
	private String serialNumber;
	private String model;
	private int batteryCapacity;
	@Positive(message = "Weight limit must be positive")
	@Max(value = 100, message = "Weight limit cannot exceed 100")
	private int weightLimit;
}
