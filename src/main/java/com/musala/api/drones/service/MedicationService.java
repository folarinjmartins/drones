package com.musala.api.drones.service;

import com.musala.api.drones.dto.ResponseDTO;
import com.musala.api.drones.dto.MedicationDTO;
import java.util.UUID;
import org.springframework.data.domain.Pageable;

public interface MedicationService {
	ResponseDTO addMedication(MedicationDTO medicationDto, byte[] image);

	MedicationDTO parseJson(String json);
}
